//
//  MenuCell.swift
//  VideoEditer
//
//  Created by Abhay on 20/03/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        
    }
    
}
