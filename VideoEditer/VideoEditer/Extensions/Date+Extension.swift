//
//  Date+Extension.swift
//  LitCircle
//
//  Created by om on 10/17/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation


extension Date
{
    func DateToString(Formatter : String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedString = dateformatter.string(from: self)
        return convertedString
    }
}
