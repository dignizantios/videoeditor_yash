//
//  UIFont+Extension.swift
//  LitCircle
//
//  Created by om on 10/16/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
   
    case medium = "SFUIDisplay-Medium"
    case heavy = "SFUIDisplay-Heavy"
    case light = "SFUIDisplay-Light"
    
}
extension UIFont
{
    
}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
   /* if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }*/
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont.systemFont(ofSize: CGFloat(size) - 2.0)
    }
    else
    {
        return UIFont.systemFont(ofSize: CGFloat(size))
    }
}

