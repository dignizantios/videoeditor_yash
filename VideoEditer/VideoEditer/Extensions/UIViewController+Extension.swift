//
//  UIViewController+Extension.swift
//  LitCircle
//
//  Created by om on 9/18/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension UIViewController
{
    
    func setUpNavigationBarWithTitle(strTitle : String,navigationColor : UIColor)
    {
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = navigationColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-90, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeBackgroundColor
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .medium)
        self.navigationItem.titleView = HeaderLabel
        
       self.navigationItem.hidesBackButton = false
        
    }
    func setUpNavigationBarWithBack(navigationColor : UIColor,title:String,btnColor:UIColor)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()

        //self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = navigationColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        
        //let leftButton = UIBarButtonItem(title: "About", style: .plain, target: self, action: #selector(backButtonTapped))
        
           // UIBarButtonItem(image: UIImage(named: "ic_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
//        let leftButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backButtonTapped))
        let leftButton = UIButton(type: .system)
        
        
        leftButton.tintColor = btnColor
        leftButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        leftButton.setTitle("", for: .normal)
        leftButton.setImage(UIImage(named: "ic_back"), for: .normal)
        
        leftButton.transform = CGAffineTransform.identity
        leftButton.sizeToFit()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftButton)
        
        if title != ""{
            let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
            HeaderLabel.isUserInteractionEnabled = false
            HeaderLabel.text = title
            HeaderLabel.textColor = UIColor.appThemeBackgroundColor
            HeaderLabel.numberOfLines = 2
            HeaderLabel.textAlignment = .center
            HeaderLabel.font = themeFont(size: 18, fontname: .medium)
            self.navigationItem.titleView = HeaderLabel
        }
        
      
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    func setNavigationBarTransparent()
    {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
        
        self.removeNavigationCustomView()
    }
    
    
    func removeNavigationCustomView()
    {
        var yValue : CGFloat = 20
        
        if UIScreen.main.bounds.height >= 812
        {
            yValue = 44
        }
        else
        {
            yValue = 20
        }
        
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: yValue, width: self.view.bounds.width, height: 0)
        
        if let viewWithTag = self.navigationController?.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
            
        }
    }

    @objc func backButtonTapped()
    {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - Textfield Done button (NumberPad)
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appNavigationBarColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
    //MARK:- share
    func shareEvent(shareLink : String,shareText:String,imgvw:UIImageView,isFromDetails:Bool)
    {
        print("Share Called")
        
        let url = URL(string: shareLink)
        let image = imgvw.image
        
        if(isFromDetails)
        {
            let vc = UIActivityViewController(activityItems: [shareText,shareLink,image], applicationActivities: [])
            self.present(vc, animated: true)
        }else{
            let vc = UIActivityViewController(activityItems: [shareText,shareLink], applicationActivities: [])
            self.present(vc, animated: true)
        }
        
        
//        let data = try? Data(contentsOf: url!)
        
     /*   DispatchQueue.main.async(execute: {
            
            let data = try? Data(contentsOf: url!)
            if let imageData = data {
                let image = UIImage(data: imageData) ?? #imageLiteral(resourceName: "image_placeholder_listing_screen")
                
                let vc = UIActivityViewController(activityItems: [shareText,image], applicationActivities: [])
                self.present(vc, animated: true)
            }
            
            
        })*/
        
        /*if let imageData = data {
            let image = UIImage(data: imageData) ?? #imageLiteral(resourceName: "image_placeholder_listing_screen")
            
            let vc = UIActivityViewController(activityItems: [shareText,image], applicationActivities: [])
            present(vc, animated: true)
        }*/
    }
    func shareEventFeeds(shareLink : String,shareText:String,image:UIImage,isFromDetails:Bool)
    {
        print("Share Called")
        
//        let url = URL(string: shareLink)
//        let image = imgvw.image
        
        
        let vc = UIActivityViewController(activityItems: [shareText,shareLink,image], applicationActivities: [])
        self.present(vc, animated: true)
        
        
    }
    
    func showAlert(message: String?) {
        let alert = UIAlertController(title: "Local Notifiucation", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            // self.navigationController?.popViewController(animated: true)
        }
        // let action = UIAlertAction(title: mapping.string(forKey: "Ok_key"), style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
}

extension UIViewController : NVActivityIndicatorViewable
{
    func showLoader()
    {
        let LoaderString:String = "Loading..."
        let LoaderType:Int = 32
        let LoaderSize = CGSize(width: 30, height: 30)
        
        startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
    }
    func hideLoader()
    {
        stopAnimating()
    }
}

//MARK:-  NavigationActionVC
class NavigationHomeVC: UINavigationController {
    
}
class NavigationOrderVC: UINavigationController {
    
}
class NavigationLikesVC: UINavigationController {
    
}
class NavigationProfileVC: UINavigationController {
    
}
