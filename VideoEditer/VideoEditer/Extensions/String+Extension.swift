//
//  String+Extension.swift
//  LitCircle
//
//  Created by om on 10/17/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation

extension String
{
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9","*"]
        return Set(self).isSubset(of: nums)
    }
    func StringToDate(Formatter : String) -> Date
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedDate = dateformatter.date(from: self)
        return convertedDate!
    }
    func StringToConvertedStringDate(strDateFormat : String,strRequiredFormat:String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = strDateFormat
        guard let convertedDate = dateformatter.date(from: self) else {
            return ""
        }
        dateformatter.dateFormat = strRequiredFormat
        let convertedString = dateformatter.string(from: convertedDate)
        return convertedString
        
    }
    public func toURL() -> URL? {
        return URL(string: self)
    }
    
    func trimmed() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    func encodedURLString() -> String
    {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        return escapedString ?? self
    }
    func isEmail() throws -> Bool {
        let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: [.caseInsensitive])
        return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
    }
    func isValidEmail() -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
        if valid {
            valid = !self.contains("..")
        }
        return valid
    }
    
    func isAlphaSpace() throws -> Bool {
        let regex = try NSRegularExpression(pattern: "^[A-Za-z ]*$", options: [])
        return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
}

