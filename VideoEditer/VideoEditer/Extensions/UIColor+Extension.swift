//
//  UIColor+Extension.swift
//  LitCircle
//
//  Created by om on 9/18/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    
    static var appNavigationBarColor : UIColor { return UIColor.init(red: 222.0/255.0, green: 39.0/255.0, blue: 139.0/255.0, alpha: 1.0) }
    static var appThemeBackgroundColor : UIColor { return UIColor.init(red: 239.0/255.0, green: 239.0/255.0, blue: 245.0/255.0, alpha: 1.0) }
    static var appThemeGreenColor : UIColor { return UIColor.init(red: 102.0/255.0, green: 156.0/255.0, blue: 36.0/255.0, alpha: 1.0) }
    static var appThemeBlackColor : UIColor { return UIColor.init(red: 38.0/255.0, green: 38.0/255.0, blue: 40.0/255.0, alpha: 1.0) }
    static var appThemeGrayColor : UIColor { return UIColor.init(red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 1.0) }
    static var appThemeRattingColor : UIColor { return UIColor.init(red: 240.0/255.0, green: 190.0/255.0, blue: 0.0/255.0, alpha: 1.0) }
     static var appFBButtonColor : UIColor { return UIColor.init(red: 38.0/255.0, green: 114.0/255.0, blue: 203.0/255.0, alpha: 1.0) }

    
    
}

