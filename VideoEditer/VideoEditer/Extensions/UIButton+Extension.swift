//
//  UIButton+Extension.swift
//  LitCircle
//
//  Created by om on 10/16/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import UIKit

extension UIButton
{
    func setUpThemeButtonUI()
    {
        self.backgroundColor = UIColor.appThemeGreenColor
        self.titleLabel?.font = themeFont(size: 16, fontname: themeFonts.medium)
        self.setTitleColor(UIColor.white, for: .normal)

    }
    func shake(duration: TimeInterval = 0.2, shakeCount: Float = 6, xValue: CGFloat = 12, yValue: CGFloat = 0,controller : UIViewController)
    {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration
        animation.repeatCount = shakeCount
        animation.autoreverses = true
        animation.delegate = controller as! CAAnimationDelegate
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - xValue, y: self.center.y - yValue))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + xValue, y: self.center.y - yValue))
        self.layer.add(animation, forKey: "shake")
        
        

    }
}
