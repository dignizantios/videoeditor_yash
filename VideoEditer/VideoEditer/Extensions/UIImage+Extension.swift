//
//  UIImage+Extension.swift
//  LitCircle
//
//  Created by om on 11/3/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import UIKit


extension UIImageView
{
   
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
    
}
