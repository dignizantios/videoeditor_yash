//
//  ViewController.swift
//  VideoEditer
//
//  Created by Abhay on 20/03/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var MenuListCollectionView: UICollectionView!
    
     //MARK:- Declaration
    
//    let MenuList = [["title":"Video Cutter","value":"ic_plash_holder"],["title":"Video Marge","value":"ic_plash_holder"],["title":"Save Video","value":"ic_plash_holder"],["title":"Setting","value":"ic_plash_holder"]]
    let MenuList = [["title":"Video Cutter","value":"ic_cut_icon"],["title":"Saved Video","value":"ic_save_icon"],["title":"Setting","value":"ic_setting_icon"]]

     //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
         decoreUI()
    }
    //MARK:- Custom Methods
    
    func decoreUI(){
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
        setUpNavigationBarWithTitle(strTitle: "Video Editor", navigationColor:  UIColor.appNavigationBarColor)
        MenuListCollectionView.delegate = self
        MenuListCollectionView.dataSource = self
    }


}
//MARK:- Collectionview Methods

extension ViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return MenuList.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath as IndexPath) as! MenuCell
        let dic = MenuList[indexPath.row] as NSDictionary
        cell.lblTitle.text = dic.object(forKey: "title") as? String
        cell.imgIcon.image = UIImage(named: dic.object(forKey: "value") as! String)
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.row == 0{
            let GalleryVideoVC = objStoryBoard.instantiateViewController(withIdentifier: "GalleryVideoVC") as! GalleryVideoVC
            self.navigationController?.pushViewController(GalleryVideoVC, animated: true)
        }
        else if indexPath.row == 1{
            let GalleryVideoVC = objStoryBoard.instantiateViewController(withIdentifier: "SavedVideosVC") as! SavedVideosVC
            self.navigationController?.pushViewController(GalleryVideoVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let noOfCellsInRow = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: 150)
        
    }
    
}
