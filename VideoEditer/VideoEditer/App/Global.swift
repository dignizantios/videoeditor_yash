//
//  Global.swift
//  LitCircle
//
//  Created by om on 9/18/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import MaterialComponents

let objStoryBoard : UIStoryboard = UIStoryboard(name:"Main",bundle:nil)
var ScreenWidth = CGFloat()
var ScreenHeight = CGFloat()

let kBasicUserName = ""
let kBasicPassword = ""



struct GlobalVariables
{
    

    static var Defaults = UserDefaults.standard
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    

    
}


func makeToast(strMessage:String)
{
    let message = MDCSnackbarMessage()
    message.text = strMessage
    MDCSnackbarManager.show(message)
}

//MARK:-
func getCurrentTimeZone() -> String
{
    var localTimeZoneName: String
    {
        return TimeZone.current.identifier
    }
    return localTimeZoneName // "America/Sao_Paulo"
}
//MARK:- Get user detail saved in UserDefaults
/// Get particular user detail saved in UserDefaults
///
/// - Parameter forKey: key in json reponse which value need.
/// - Returns:  string value after finding from response according to forKey


func getDefaultsValue(key : String) -> String
{
    return GlobalVariables.Defaults.value(forKey: key) as? String ?? ""
}
func setValueToDefaults(key : String,value:Any)
{
    GlobalVariables.Defaults.set(value, forKey: key)
    GlobalVariables.Defaults.synchronize()
}

func StringToDate(Formatter : String,strDate : String) -> Date
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    
    guard let convertedDate = dateformatter.date(from: strDate) else {
        let str = dateformatter.string(from: Date())
        return dateformatter.date(from: str)!
        
    }
    //print("convertedDate - ",convertedDate)
    return convertedDate
}
func DateToString(Formatter : String,date : Date) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    let convertedString = dateformatter.string(from: date)
    return convertedString
}
func StringToConvertedStringDate(strDate : String,strDateFormat : String,strRequiredFormat:String) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = strDateFormat
    guard let convertedDate = dateformatter.date(from: strDate) else {
        return ""
    }
    dateformatter.dateFormat = strRequiredFormat
    let convertedString = dateformatter.string(from: convertedDate)
    return convertedString
    
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}

func convertToDictionary(text: String) -> NSMutableDictionary
{
    var dict = NSMutableDictionary()
    if let data = text.data(using: .utf8)
    {
        do
        {
            dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            return dict
        } catch
        {
            print(error.localizedDescription)
        }
    }
    return dict
}
