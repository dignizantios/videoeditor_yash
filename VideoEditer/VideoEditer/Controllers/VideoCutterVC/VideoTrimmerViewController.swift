//
//  ViewController.swift
//  PryntTrimmerView
//
//  Created by Henry on 27/03/2017.
//  Copyright © 2017 Prynt. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import PryntTrimmerView
import AssetsLibrary

/// A view controller to demonstrate the trimming of a video. Make sure the scene is selected as the initial
// view controller in the storyboard
class VideoTrimmerViewController: AssetSelectionViewController {

    @IBOutlet weak var selectAssetButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var trimmerView: TrimmerView!

    var player: AVPlayer?
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarWithBack(navigationColor: UIColor.appNavigationBarColor, title: "Crop Video", btnColor: UIColor.white)

        trimmerView.handleColor = UIColor.white
        trimmerView.mainColor = UIColor.appNavigationBarColor
         loadSelectedAsset()
        print(trimmerView.startTime?.seconds)
        print(trimmerView.endTime?.seconds)
    }

    @IBAction func selectAsset(_ sender: Any) {
        loadAssetRandomly()
    }

    @IBAction func play(_ sender: Any) {

        guard let player = player else { return }

        if !player.isPlaying {
            player.play()
            startPlaybackTimeChecker()
        } else {
            player.pause()
            stopPlaybackTimeChecker()
        }
    }

    override func loadAsset(_ asset: AVAsset) {

        trimmerView.asset = asset
        trimmerView.delegate = self
        addVideoPlayer(with: asset, playerView: playerView)
    }

    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)

        NotificationCenter.default.addObserver(self, selector: #selector(VideoTrimmerViewController.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)

        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
    }

    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = trimmerView.startTime {
            player?.seek(to: startTime)
        }
    }

    func startPlaybackTimeChecker() {

        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(VideoTrimmerViewController.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }

    func stopPlaybackTimeChecker() {

        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }

    @objc func onPlaybackTimeChecker() {

        guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime, let player = player else {
            return
        }

        let playBackTime = player.currentTime()
        trimmerView.seek(to: playBackTime)

        if playBackTime >= endTime {
            player.seek(to: startTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            trimmerView.seek(to: startTime)
        }
    }
    
    @IBAction func btnSaveClicked(_ sender: Any) {
        print(trimmerView.startTime)
        print(trimmerView.endTime)
        self.showProgress()
//        self.cropVideo(sourceURL: SelectedVideoPathURL!, start: trimmerView!.startTime!, duration: trimmerView!.endTime!)
        self.cropVideo(sourceURL: SelectedVideoPathURL!, startTime: (trimmerView.startTime?.seconds)!, endTime: (trimmerView.endTime?.seconds)!) { (url) in
            print(url)
            self.SaveVideoToPhotoLibrary(outputFileURL: url as NSURL)
        }
    }
    
    
    func cropVideo(sourceURL: URL, startTime: Double, endTime: Double, completion: ((_ outputUrl: URL) -> Void)? = nil)
    {
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        let asset = AVAsset(url: sourceURL)
        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        print("video length: \(length) seconds")
        
        var outputURL = documentDirectory.appendingPathComponent("output")
        do {
            try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(sourceURL.lastPathComponent).mp4")
        }catch let error {
            print(error)
        }
        
        //Remove existing file
        try? fileManager.removeItem(at: outputURL)
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        
        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                                    end: CMTime(seconds: endTime, preferredTimescale: 1000))
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                print("exported at \(outputURL)")
                completion?(outputURL)
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }
    
    func cropVideo(sourceURL: URL, start: CMTime, duration: CMTime)
    {
        let asset = AVURLAsset(url: sourceURL as URL, options: nil)
        
        let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)
        var error : NSError?
        let file = "Finaloutput.mp4"
        /*   let paths : AnyObject = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0]
         let outputURL1 = paths[0] as? String*/
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        
        let outputURL1 = paths[0] as? String
        
        let filemgr = FileManager.default
        do
        {
            print(outputURL1)
            try filemgr.createDirectory(atPath: outputURL1!, withIntermediateDirectories: true, attributes: nil)
        }
        catch
        {
            print("Something went wrong here.")
            // write some code to recover from error
        }
        
        var outputURL = outputURL1!.appending(file)
        //stringByAppendingPathComponent(file)
        do
        {
            try  filemgr.removeItem(atPath: outputURL)
            //removeItemAtPath(outputURL, error: &error)
        }
        catch
        {
            print("Something went wrong here.")
            // write some code to recover from error
        }
        // let FinalUrlTosave = NSURL(string: outputURL)
        let FinalUrlTosave = NSURL(fileURLWithPath: outputURL)
        exportSession?.outputURL=FinalUrlTosave as URL
        exportSession?.shouldOptimizeForNetworkUse = true
        // exportSession.outputFileType = AVFileTypeQuickTimeMovie
        exportSession?.outputFileType = AVFileType.mov;
//        let start:CMTime
//        let duration:CMTime
//        start = CMTimeMakeWithSeconds(startTime, preferredTimescale: 600)
//        duration = CMTimeMakeWithSeconds(endTime, preferredTimescale: 600)
        // let timeRangeForCurrentSlice = CMTimeRangeMake(start, duration)
        let range = CMTimeRangeMake(start: start, duration: duration);
        exportSession?.timeRange = range
        
        let destinationURL1 = NSURL(string: outputURL)
        // on exportSession completed
        exportSession?.exportAsynchronously(completionHandler: {
            //exportProgressBarTimer.invalidate(); // remove/invalidate timer
            print(exportSession?.status)
            if exportSession?.status == AVAssetExportSession.Status.completed {
                // [....Some Completion Code Here]
                DispatchQueue.main.async
                    {
                        self.SaveVideoToPhotoLibrary(outputFileURL: destinationURL1!)
                        /* let asset = AVAsset(url: sourceURL as URL)
                         print("asset-->",asset)
                         self.objWaterMark.watermark(video: asset, imageName: "ic_map_icon", saveToLibrary: true, watermarkPosition: .TopRight, completion: { (status : AVAssetExportSession.Status!, session: AVAssetExportSession!, outputURL : NSURL!) in
                         
                         print("WaterMarkUrl-->",outputURL)
                         })*/
                        
                        
                }
                
            }
        })
        /*
         exportSession?.exportAsynchronously(completionHandler: {
         switch exportSession?.status{
         case  AVAssetExportSession.Status.failed:
         print("failed \(exportSession?.error)")
         case AVAssetExportSession.Status.cancelled:
         print("cancelled \(exportSession?.error)")
         default:
         print("complete....complete")
         self.SaveVideoToPhotoLibrary(outputFileURL: destinationURL1!)
         
         }
         })*/
    }
    
    func SaveVideoToPhotoLibrary(outputFileURL: NSURL)
        
    {
        let assetsLibrary = ALAssetsLibrary()
        
        let videoURL = outputFileURL as NSURL?
        
        if let library = assetsLibrary as? ALAssetsLibrary{
            
            if let url = videoURL{
                
                library.writeVideoAtPath(toSavedPhotosAlbum: url as URL, completionBlock: { (assetURL: URL?, error: Error?) -> Void in
                    DispatchQueue.main.sync {
                        self.removeProgress()
                        if (error != nil) {
                            let alertView:UIAlertView = UIAlertView()
                            alertView.title = "Failed!"
                            alertView.message = error!.localizedDescription
                            alertView.delegate = self
                            alertView.addButton(withTitle: "OK")
                            alertView.show()
                        } else {
                            if let validAssetURL = assetURL {
                                self.saveAllSavedVideo(str: (assetURL?.absoluteString)!)
                                let alertView:UIAlertView = UIAlertView()
                                alertView.title = "Saved"
                                alertView.message = nil
                                alertView.delegate = self
                                alertView.addButton(withTitle: "OK")
                                alertView.show()
                            }
                        }
                    }
                })                
                
            } else {
                self.removeProgress()
                print("Could not find the video in the app bundle")
            }
            
        }
        
        
    }
}

extension VideoTrimmerViewController: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        player?.play()
        startPlaybackTimeChecker()
    }

    func didChangePositionBar(_ playerTime: CMTime) {
        stopPlaybackTimeChecker()
        player?.pause()
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (trimmerView.endTime! - trimmerView.startTime!).seconds
        print(duration)
    }
}

extension UIViewController {
    
    func getAllSavedVideo() -> [String] {
        let defaults = UserDefaults.standard
        let array = defaults.object(forKey: "SavedStringArray") as? [String] ?? [String]()
        return array
    }
    
    func saveAllSavedVideo(str: String) {
        var array = getAllSavedVideo()
        array.append(str)
        let defaults = UserDefaults.standard
        defaults.set(array, forKey: "SavedStringArray")
    }
}
