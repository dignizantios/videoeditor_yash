//
//  SavedVideosVC.swift
//  VideoEditer
//
//  Created by Haresh Bhai on 23/03/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import AssetsLibrary
import Photos

class SavedVideosVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarWithBack(navigationColor: UIColor.appNavigationBarColor, title: "Saved Video", btnColor: UIColor.white)
        print(self.getAllSavedVideo())
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getAllSavedVideo().count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GallaryVideoCell = tableView.dequeueReusableCell(withIdentifier: "GallaryVideoCell") as! GallaryVideoCell
        
//        let asset = AVURLAsset(url: URL(string: self.getAllSavedVideo().first!)!)
//        print(asset.duration.seconds)        
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
