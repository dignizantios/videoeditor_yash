//
//  GalleryVideoVC.swift
//  VideoEditer
//
//  Created by Abhay on 20/03/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import Photos
import NVActivityIndicatorView

class GalleryVideoVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var GalleryListTableView: UITableView!
    
    //MARK:- Declaration
    var photos: PHFetchResult<PHAsset>!
    
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        decoreUI()
    }
    //MARK:- Custom Methods
    
    func decoreUI(){
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
        setUpNavigationBarWithBack(navigationColor: UIColor.appNavigationBarColor, title: "Select Video", btnColor: UIColor.white)
        GalleryListTableView.delegate = self
        GalleryListTableView.dataSource = self
         getAssetFromPhoto()
    }
    // get all videos from Photos library you need to import Photos framework
    
    func getAssetFromPhoto() {
        let options = PHFetchOptions()
        options.sortDescriptors = [ NSSortDescriptor(key: "creationDate", ascending: true) ]
        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        photos = PHAsset.fetchAssets(with: options)
        print(photos)
        GalleryListTableView.reloadData()
        print(photos.count)
    }

}
extension GalleryVideoVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GallaryVideoCell") as! GallaryVideoCell
        let asset = photos!.object(at: indexPath.row)
        print("asset-->",asset.duration)
        let width: CGFloat = 50
        let height: CGFloat = 50
        let size = CGSize(width:width, height:height)
        PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, userInfo) -> Void in
            cell.imgVideo.image = image
            cell.lblDuration.text = String(format: "%02d:%02d",Int((asset.duration / 60)),Int(asset.duration) % 60)
            
        }
        let options: PHVideoRequestOptions = PHVideoRequestOptions()
        options.version = .original
        PHImageManager.default().requestAVAsset(forVideo: asset, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
            if let urlAsset = asset as? AVURLAsset {
                let localVideoUrl: URL = urlAsset.url as URL
                print("Video Url-->",localVideoUrl)
            } else {
                print("Video Url not found")
            }
        })
         cell.lblSize.text = ""
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.showProgress()
         let asset = photos!.object(at: indexPath.row)
        let options: PHVideoRequestOptions = PHVideoRequestOptions()
        options.version = .original
        PHImageManager.default().requestAVAsset(forVideo: asset, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
            if let urlAsset = asset as? AVURLAsset {
                let localVideoUrl: URL = urlAsset.url as URL
                print("Video Url-->",localVideoUrl)
                DispatchQueue.main.sync {
                    self.removeProgress()
                    let VideoTrimmerViewController = objStoryBoard.instantiateViewController(withIdentifier: "trimmerViewController") as! VideoTrimmerViewController
                    VideoTrimmerViewController.fetchResult = self.photos!
                    VideoTrimmerViewController.selectedIndex = indexPath.row
                    VideoTrimmerViewController.SelectedVideoPathURL = localVideoUrl
                    VideoTrimmerViewController.SelectedVideoPath = localVideoUrl.absoluteString
                    print("SelectedVideoPath-->",VideoTrimmerViewController.SelectedVideoPath)
                    self.navigationController?.pushViewController(VideoTrimmerViewController, animated: true)
                }
            } else {
                print("Video Url not found")
                self.stopAnimating()
            }
        })
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

extension UIViewController {
    func showProgress() {
        let LoaderString:String = "Loading..."
        let LoaderType:Int = 32
        let LoaderSize = CGSize(width: 30, height: 30)
        self.view.isUserInteractionEnabled = false
        self.startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
    }
    
    func removeProgress() {
        self.view.isUserInteractionEnabled = true
        self.stopAnimating()
    }
}
