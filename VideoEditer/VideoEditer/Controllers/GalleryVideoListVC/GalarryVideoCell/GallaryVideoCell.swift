//
//  GallaryVideoCell.swift
//  VideoEditer
//
//  Created by Abhay on 20/03/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class GallaryVideoCell: UITableViewCell {

    @IBOutlet weak var imgVideo: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
